# -*- mode: python -*-

block_cipher = None


a = Analysis(['pnc-blockly.py'],
             pathex=['/home/tuankiet65/PycharmProjects/pnc-blockly'],
             binaries=[],
             datas=[
                 ("static/dist/", "static/dist"),
                 ("static/node_modules/xterm/lib/xterm.css", "static/node_modules/xterm/lib/"),
                 ("static/node_modules/noty/lib/noty.css", "static/node_modules/noty/lib/"),
                 ("static/node_modules/blockly/", "static/node_modules/"),
                 ("templates/index.html", "templates"),
                 ("programs/", "programs")
             ],
             hiddenimports=[
                 "engineio.async_eventlet", 
                 "engineio.async_gevent", 
                 "engineio.async_threading"
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='pnc-blockly',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='pnc-blockly')
