module.exports = {
  "contextSeparator": "_",
  // Key separator used in your translation keys     

  "createOldCatalogs": true,
  // Save the \_old files                                  

  "defaultNamespace": "translation",
  // Default namespace used in your i18next config         

  "defaultValue": "",
  // Default value to give to empty keys                   

  "extension": ".json",
  // Extension of the catalogs 
  // Supports $LOCALE and $NAMESPACE injection

  "filename": "$NAMESPACE",
  // Filename of the catalogs                              
  // Supports $LOCALE and $NAMESPACE injection

  "indentation": 4,
  // Indentation of the catalog files                      

  "keepRemoved": true,
  // Keep keys from the catalog that are no longer in code 

  "keySeparator": ".",
  // Key separator used in your translation keys

  // see below for more details
  "lexers": {
    hbs: ['HandlebarsLexer'],
    handlebars: ['HandlebarsLexer'],

    htm: ['HTMLLexer'],
    html: ['HTMLLexer'],

    js: [{
      lexer: 'JavascriptLexer',
      functions: ['t'], // Array of functions to match
  
      // acorn config (for more information on the acorn options, see here: https://github.com/acornjs/acorn#main-parser)
      acorn: {
        sourceType: 'module', 
        ecmaVersion: 9, // forward compatibility
        plugins: {
          es7: true, // some es7 parsing that's not yet in acorn (decorators)
          stage3: true // load some stage3 configs not yet in a version
        }
      }
    }],

    jsx: ['JsxLexer'],
    mjs: ['JavascriptLexer'],

    default: ['JavascriptLexer']
  },

  "lineEnding": "auto",
  // Control the line ending. See options at https://github.com/ryanve/eol

  "locales": ['en', 'vi'],
  // An array of the locales in your applications          

  "namespaceSeparator": ":",
  // Namespace separator used in your translation keys   

  "output": "js/locales",
  // Where to write the locale files relative to the base

  "input": ["js/**/*.js"],
  // An array of globs that describe where to look for source files

  "reactNamespace": false,
  // For react file, extract the defaultNamespace - https://react.i18next.com/components/translate-hoc.html
  // Ignored when parsing a `.jsx` file and namespace is extracted from that file.

  "verbose": true
  // Display info about the parsing including some stats
}
