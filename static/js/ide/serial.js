import {IDETab} from './tabs';
import Terminal from 'xterm';
import io from 'socket.io-client';
import Noty from 'noty';
import i18n from "../i18n";

Terminal.loadAddon("fit");

export class IDESocketIOSerial {
    constructor (port, baud_rate){
        this.port = port;
        this.baud_rate = baud_rate;

        this.socket = io();

        this.socket.on('uart_data_received', function(data) {
            this.data_received_handler(data);
        }.bind(this));

        this.socket.on('uart_disconnected', function() {
            this.connected = false;
            let noty = new Noty({
                layout: "bottomRight",
                theme: "relax",
                type: "alert",
                timeout: 5000,
                text: i18n.t("serial.connection_closed") //"Connection to board closed"
            }).show();
        }.bind(this));

        this.connect();
        this.connect_loop_interval = setInterval(this.connect.bind(this), 5000);
    }

    on_data_received (func) { this.data_received_handler = func }

    connect (){
        if (this.connected) return;
        this.socket.emit('uart_connect', {
            "port": this.port,
            "baud_rate": this.baud_rate
        }, function(data) {
            if (!(data.err)){
                let noty = new Noty({
                    layout: "bottomRight",
                    theme: "relax",
                    type: "info",
                    timeout: 5000,
                    text: i18n.t("serial.connected")
                }).show();
            } else {
                if (this.connected) {
                    let noty = new Noty({
                        layout: "bottomRight",
                        theme: "relax",
                        type: "alert",
                        timeout: 5000,
                        text: i18n.t("serial.connect_error")
                    }).show();
                }
            }

            this.connected = !(data.err);
        }.bind(this))
    }

    close() {
        this.socket.emit('uart_disconnect', {
            "data": null
        });

        clearInterval(this.connect_loop_interval);
    }

    write(data) {
        this.socket.emit('uart_write', {
            "data": data
        });
    }

    static run_program(code) {
        let socket = io();
        socket.emit('uart_run_program', {
            "py": code
        });
    }
}

export class IDESerialMonitor extends IDETab {
    constructor(tabs_manager, name = null){
        if (name === null) {
            name = i18n.t("serial.tab_name") // "Serial monitor - /dev/ttyACM0";
        }
        super(tabs_manager, name);
        this.name = name;

        this.serial = new IDESocketIOSerial("/dev/ttyACM0", 115200);
        this.serial.on_data_received(this.write_data.bind(this));

        this.term = new Terminal();
        this.term.open(this.container[0]);
        this.term.on("key", function(key, e) {
            console.log(key);
            this.serial.write(key);
        }.bind(this));
    }

    write_data(data) {
        this.term.write(atob(data.data));
    }

    render(){
        this.term.reset();
        this.term.fit();
    }

    on_close() {
        this.serial.close();
    }
}
