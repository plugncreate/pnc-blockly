import i18n from '../i18n';
export class IDESelector {
    constructor(selections, callback) {
        this.selections = selections;
        this.callback = callback;
        this.render();
    }

    render() {
    	this.modal = $(`
			<div class="modal fade">
  				<div class="modal-dialog modal-lg" role="document">
 					<div class="modal-content">
 						<div class="modal-header">
        					<h5 class="modal-title">${i18n.t("ide.program_selector.title")}</h5>
        					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        						<span aria-hidden="true">&times;</span>
        					</button>
      					</div>
      					<div class="modal-body">
        					<div class="ide-modal-selector-list list-group">
        						<!-- To be filled in -->
        					</div>
      					</div>
      					<div class="modal-footer">
        					<button type="button" class="btn btn-secondary" data-dismiss="modal">${i18n.t("ide.program_selector.close")}</button>
      					</div>
    				</div>
  				</div>
			</div>
    	`);

    	var list_element = this.modal.find(".ide-modal-selector-list");

    	for (var selection of this.selections){
    		$(`
    			<button type="button" class="list-group-item list-group-item-action" data-value="${selection}">
    				${selection}
    			</button>
    		`).appendTo(list_element).click(this.selection_handler.bind(this));
    	}

        this.modal.appendTo($("html"));
        this.modal.modal('show');
    }

    selection_handler (e) {
    	var button = $(e.target);
        this.modal.modal('hide');
    	this.callback(button.data("value"));
    }
}
