export class IDEToolbarItem {
    constructor(icon, name, func) {
        this.icon = icon;
        this.name = name;
        this.func = func;
    }

    render(inside_category = false) {
        if (inside_category) {
            return $(`
                <a class="nav-link" href="#">
                    <i class="fa ${this.icon}"></i>
                    ${this.name}
                </a>
            `).click(this.func);
        } else {
            return $(`
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fa ${this.icon}"></i>
                        ${this.name}
                    </a>
                </li>
            `).click(this.func);
        }
    }
}

export class IDEToolbarCategory {
    constructor(name) {
        this.name = name;
        this.items = [];
    }

    add_item(item) {
        this.items.push(item);
    }

    remove_item(item) {
        var position = this.items.indexOf(item);
        if (position != -1) {
            this.items.splice(position, 1);
        }
    }

    render() {
        element = $(`
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                    ${this.name}
                </a>
                <div class="dropdown-menu">
                    <!-- To be filled in below -->
                </div>
            </li>
        `);

        var dropdown_menu_element = element.find(".dropdown-menu");

        for (var item of this.items) {
            item.render(true).appendTo(dropdown_menu_element);
        }

        return element;
    }
}

export class IDEToolbar {
    constructor(container) {
        this.items = [];
        this.container = container;
    }

    add(item) {
        this.items.push(item);
        this.render();
    }

    render() {
        var element = $(`
            <nav class="navbar navbar-expand-md navbar-dark bg-primary">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Plug and Create IDE</a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                    </ul>
                </div>
            </nav>
        `);

        var navbar_content = element.find("ul");
        for (var item of this.items) {
            item.render().appendTo(navbar_content);
        }

        this.container.html(element);
    }
}