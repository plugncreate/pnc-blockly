import {IDESocketIOSerial} from "./serial";

export class IDEBackend {
    static get_program_list(callback){
        $.getJSON(`/get_program_list`, callback);
    }

    static get_program(name, callback){
        $.getJSON(`/get_program`, {
            "name": name
        }, function (data){
            callback(data.name, data.xml, data.py);
        });
    }

    static save_program(name, xml, py, callback){
        $.post(`/save_program`, {
            "name": name,
            "xml": xml,
            "py": py
        }).done(callback);
    }

    static run_program(name, success_callback, error_callback){
        $.get(`/run_program`, {
            "name": name
        }).done(success_callback).fail(error_callback);
        // IDESocketIOSerial.run_program(code);
        // success_callback();
    }
}