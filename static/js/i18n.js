import i18next from "i18next";
import LngDetector from "i18next-browser-languagedetector";
import locales from "./locales";

const i18n = i18next.createInstance();
i18n.use(LngDetector)
    .init({
        lng: 'vi',
        resources: locales,
    });

export default i18n;
