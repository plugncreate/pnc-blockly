import {BLOCKS as blocks_dht11} from "./blocks/pnc/blocks_dht11.js";
import {BLOCKS as blocks_dht22} from "./blocks/pnc/blocks_dht22.js";
import {BLOCKS as blocks_esp} from "./blocks/pnc/blocks_esp.js";
import {BLOCKS as blocks_lcd} from "./blocks/pnc/blocks_lcd.js";
import {BLOCKS as blocks_numpad} from "./blocks/pnc/blocks_numpad.js";
import {BLOCKS as blocks_pir} from "./blocks/pnc/blocks_pir.js";
import {BLOCKS as blocks_relay2x} from "./blocks/pnc/blocks_relay2x.js";
import {BLOCKS as blocks_rtc} from "./blocks/pnc/blocks_rtc.js";
import {BLOCKS as blocks_servo} from "./blocks/pnc/blocks_servo.js";
import {BLOCKS as blocks_ultrasonic} from "./blocks/pnc/blocks_ultrasonic.js";

import {BLOCKS as blocks_pyb} from "./blocks/pyb/blocks_pyb.js";

import {IDETab} from "../ide/tabs";

import {IDEBackend} from "../ide/backend";

import Noty from "noty";

import i18n from '../i18n';

export class IDEBlockly extends IDETab {

    constructor(tabs_manager, name = null, xml = null){
        var is_untitled = false;

        if (name === null) {
            name = i18n.t("blockly.untitled_program");
            is_untitled = true;    
        }

        super(tabs_manager, name);

        this.name = name;

        this.is_new = false;
        if (is_untitled) {
            this.is_new = true;
        }

        this.init_toolbox();
    }

    init_toolbox(){
        this.toolbox = $($.parseXML(`<xml id="toolbox" style="display: none">
    <category name="Logic">
        <block type="controls_if"></block>
        <block type="logic_compare"></block>
        <block type="logic_operation"></block>
        <block type="logic_negate"></block>
        <block type="logic_boolean"></block>
        <block type="logic_null"></block>
        <block type="logic_ternary"></block>
    </category>
    <category name="Loops">
        <block type="controls_repeat_ext">
            <value name="TIMES">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block type="controls_whileUntil"></block>
        <block type="controls_for">
            <value name="FROM">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="TO">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
            <value name="BY">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
        </block>
        <block type="controls_forEach"></block>
        <block type="controls_flow_statements"></block>
    </category>
    <category name="Maths">
        <block type="math_number"></block>
        <block type="math_arithmetic">
            <value name="A">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="B">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
        </block>
        <block type="math_single">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM">9</field>
                </shadow>
            </value>
        </block>
        <block type="math_trig">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM">45</field>
                </shadow>
            </value>
        </block>
        <block type="math_constant"></block>
        <block type="math_number_property">
            <value name="NUMBER_TO_CHECK">
                <shadow type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        <block type="math_round">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM">3.1</field>
                </shadow>
            </value>
        </block>
        <block type="math_on_list"></block>
        <block type="math_modulo">
            <value name="DIVIDEND">
                <shadow type="math_number">
                    <field name="NUM">64</field>
                </shadow>
            </value>
            <value name="DIVISOR">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block type="math_constrain">
            <value name="VALUE">
                <shadow type="math_number">
                    <field name="NUM">50</field>
                </shadow>
            </value>
            <value name="LOW">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="HIGH">
                <shadow type="math_number">
                    <field name="NUM">100</field>
                </shadow>
            </value>
        </block>
        <block type="math_random_int">
            <value name="FROM">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="TO">
                <shadow type="math_number">
                    <field name="NUM">100</field>
                </shadow>
            </value>
        </block>
        <block type="math_random_float"></block>
    </category>
    <category name="Text">
        <block type="text"></block>
        <block type="text_join"></block>
        <block type="text_append">
            <value name="TEXT">
                <shadow type="text"></shadow>
            </value>
        </block>
        <block type="text_length">
            <value name="VALUE">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
        <block type="text_isEmpty">
            <value name="VALUE">
                <shadow type="text">
                    <field name="TEXT"></field>
                </shadow>
            </value>
        </block>
        <block type="text_indexOf">
            <value name="VALUE">
                <block type="variables_get">
                    <field name="VAR">{textVariable}</field>
                </block>
            </value>
            <value name="FIND">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
        <block type="text_charAt">
            <value name="VALUE">
                <block type="variables_get">
                    <field name="VAR">{textVariable}</field>
                </block>
            </value>
        </block>
        <block type="text_getSubstring">
            <value name="STRING">
                <block type="variables_get">
                    <field name="VAR">{textVariable}</field>
                </block>
            </value>
        </block>
        <block type="text_changeCase">
            <value name="TEXT">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
        <block type="text_trim">
            <value name="TEXT">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
        <block type="text_print">
            <value name="TEXT">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
        <block type="text_prompt_ext">
            <value name="TEXT">
                <shadow type="text">
                    <field name="TEXT">abc</field>
                </shadow>
            </value>
        </block>
    </category>
    <category name="Lists">
        <block type="lists_create_with">
            <mutation items="0"></mutation>
        </block>
        <block type="lists_create_with"></block>
        <block type="lists_repeat">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM">5</field>
                </shadow>
            </value>
        </block>
        <block type="lists_length"></block>
        <block type="lists_isEmpty"></block>
        <block type="lists_indexOf">
            <value name="VALUE">
                <block type="variables_get">
                    <field name="VAR">{listVariable}</field>
                </block>
            </value>
        </block>
        <block type="lists_getIndex">
            <value name="VALUE">
                <block type="variables_get">
                    <field name="VAR">{listVariable}</field>
                </block>
            </value>
        </block>
        <block type="lists_setIndex">
            <value name="LIST">
                <block type="variables_get">
                    <field name="VAR">{listVariable}</field>
                </block>
            </value>
        </block>
        <block type="lists_getSublist">
            <value name="LIST">
                <block type="variables_get">
                    <field name="VAR">{listVariable}</field>
                </block>
            </value>
        </block>
        <block type="lists_split">
            <value name="DELIM">
                <shadow type="text">
                    <field name="TEXT">,</field>
                </shadow>
            </value>
        </block>
        <block type="lists_sort"></block>
    </category>
    <sep></sep>
    <sep></sep>
    <category name="Variables" custom="VARIABLE"></category>
    <category name="Functions" custom="PROCEDURE"></category>
</xml>`));

        this.add_category(blocks_pyb);

        this.add_category(blocks_dht11);
        this.add_category(blocks_dht22);
        this.add_category(blocks_esp);
        this.add_category(blocks_lcd);
        this.add_category(blocks_numpad);
        this.add_category(blocks_pir);
        this.add_category(blocks_relay2x);
        this.add_category(blocks_rtc);
        this.add_category(blocks_servo);
        this.add_category(blocks_ultrasonic);
    }

    add_category(category){
        var xml = $(`
    		<category name="${category.name}">
    			<!-- To be filled in -->
    		</category>
    	`);

        for (var block_name in category.blocks) {
            if (category.blocks.hasOwnProperty(block_name)) {
                var block_id = `${category.id_prefix}_${block_name}`;

                Blockly.Blocks[block_id] = {
                    init: category.blocks[block_name].init
                };

                Blockly.Python[block_id] = category.blocks[block_name].generator_py;

                xml.append(`
    				<block type="${block_id}"></block>
    			`);
            }
        }

        this.toolbox.find("sep:eq(1)").before(xml);
    }

    render(){
        this.workspace = Blockly.inject(this.container[0], {
            toolbox: (new XMLSerializer().serializeToString(this.toolbox[0])),
            media: "static/node_modules/blockly/media/"
        });

        if (!this.is_new) {
            IDEBackend.get_program(this.name, function (name, xml, py){
                Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(xml), this.workspace);
            }.bind(this));
        }

        Blockly.svgResize(this.workspace);
    }

    on_save(){
        if (this.is_new) {
            this.title = prompt(i18n.t("blockly.on_save.program_name"));

            if (!this.title) {
                return;
            }

            this.is_new = false;

            this.tabs_manager.render();
        }

        var xml_text = Blockly.Xml.domToPrettyText(Blockly.Xml.workspaceToDom(this.workspace));
        var py_code = Blockly.Python.workspaceToCode(this.workspace);

        IDEBackend.save_program(this.title, xml_text, py_code, function (){
            var noty = new Noty({
                layout: "bottomRight",
                theme: "relax",
                type: "success",
                timeout: 5000,
                text: i18n.t("blockly.on_save.save_success")
            }).show();
        });
    }

    on_upload(){
        let xml_text = Blockly.Xml.domToPrettyText(Blockly.Xml.workspaceToDom(this.workspace));
        let py_code = Blockly.Python.workspaceToCode(this.workspace);

        if (this.title === i18n.t("blockly.untitled_program")) {
            this.on_save();
        }

        IDEBackend.save_program(this.title, xml_text, py_code, function (){
            IDEBackend.run_program(this.title, function (){
                let noty = new Noty({
                    layout: "bottomRight",
                    theme: "relax",
                    type: "success",
                    timeout: 5000,
                    text: i18n.t("blockly.on_upload.success")
                }).show();
            }, function (){
                let noty = new Noty({
                    layout: "bottomRight",
                    theme: "relax",
                    type: "error",
                    timeout: 5000,
                    text: i18n.t("blockly.on_upload.fail")
                }).show();
            });
        }.bind(this));
    }

    on_close(){
        this.on_save();
    }
}
