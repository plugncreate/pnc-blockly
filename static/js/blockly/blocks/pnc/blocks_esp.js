import i18n from "../../../i18n";

export var BLOCKS = {
    name: "ESP8266",
    id_prefix: "pnc_esp8266",
    blocks: {}
};

BLOCKS.blocks['new'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("esp.new.description"))
            .appendField(new Blockly.FieldDropdown([
                    ["UART1", "UART1"],
                    ["UART2", "UART2"],
                    ["UART3", "UART3"]]),
                "port")
            .appendField(i18n.t("esp.new.ssid"))
            .appendField(new Blockly.FieldTextInput(""), "ssid")
            .appendField(i18n.t("esp.new.password"))
            .appendField(new Blockly.FieldTextInput(""), "password");
        this.setOutput(true, "ESP8266");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_port = block.getFieldValue('port');
        var text_ssid = block.getFieldValue('ssid');
        var text_password = block.getFieldValue('password');

        var code = `pnc.ESP8266("${dropdown_port}", "${text_ssid}", "${text_password}")`;

        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['get'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("esp.get.description"));
        this.appendValueInput("esp_object")
            .setCheck("ESP8266")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.get.esp_object"));
        this.appendValueInput("host")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.get.host"));
        this.appendValueInput("path")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.get.path"));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var value_esp_object = Blockly.Python.valueToCode(block, 'esp_object', Blockly.Python.ORDER_ATOMIC);
        var value_host = Blockly.Python.valueToCode(block, 'host', Blockly.Python.ORDER_ATOMIC);
        var value_path = Blockly.Python.valueToCode(block, 'path', Blockly.Python.ORDER_ATOMIC);

        var code = `${value_esp_object}.get(${value_host}, 80, ${value_path})\n`;

        return code;
    }
};

BLOCKS.blocks['thingspeak_update_channel'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("esp.thinkspeak_update.description"));
        this.appendValueInput("esp_object")
            .setCheck("ESP8266")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.thinkspeak_update.object"));
        this.appendValueInput("api_key")
            .setCheck("String")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.thinkspeak_update.api_key"));
        this.appendValueInput("field1")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("esp.thinkspeak_update.field1"));
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var value_esp_object = Blockly.Python.valueToCode(block, 'esp_object', Blockly.Python.ORDER_ATOMIC);
        var value_api_key = Blockly.Python.valueToCode(block, 'api_key', Blockly.Python.ORDER_ATOMIC);
        var value_field1 = Blockly.Python.valueToCode(block, 'field1', Blockly.Python.ORDER_ATOMIC);

        var code = `pnc.ThingSpeakChannel(${value_esp_object}, ${value_api_key}).update(1, ${value_field1})\n`;

        return code;
    }
};
