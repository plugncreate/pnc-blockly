import i18n from "../../../i18n";

export let BLOCKS = {
    name: "Servo",
    id_prefix: "pnc_servo",
    blocks: {}
};

BLOCKS.blocks['servo_angle_as_angle'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("servo.change_angle"))
            .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"]]), "servo_num")
            .appendField(i18n.t("servo.on_port"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port")
            .appendField(i18n.t("servo.to"))
            .appendField(new Blockly.FieldAngle(90), "angle");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_servo_num = block.getFieldValue('servo_num');
        var dropdown_port = block.getFieldValue('port');
        var number_angle = block.getFieldValue('angle');

        var code = `pnc.Servo("${dropdown_port}").angle(${dropdown_servo_num}, ${number_angle})\n`;

        return code;
    }
};

BLOCKS.blocks['servo_angle_as_block'] = {
    init: function (){
        this.appendValueInput("angle")
            .setCheck("Number")
            .appendField(i18n.t("servo.change_angle"))
            .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"]]), "servo_num")
            .appendField(i18n.t("servo.on_port"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port")
            .appendField(i18n.t("servo.to"));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_servo_num = block.getFieldValue('servo_num');
        var dropdown_port = block.getFieldValue('port');
        var number_angle = Blockly.Python.valueToCode(block, 'angle', Blockly.Python.ORDER_ATOMIC);

        var code = `pnc.Servo("${dropdown_port}").angle(${dropdown_servo_num}, ${number_angle})\n`;

        return code;
    }
};
