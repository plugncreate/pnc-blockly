import i18n from "../../../i18n";

export let BLOCKS = {
    name: "PIR",
    id_prefix: "pnc_pir",
    blocks: {}
};

BLOCKS.blocks['pir_detect'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("pir.detect"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setOutput(true, "Boolean");
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        var code = `(pnc.PIR("${dropdown_port}").detect())`;
        return [code, Blockly.Python.ORDER_NONE];
    }
};
