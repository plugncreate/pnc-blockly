import i18n from '../../../i18n';

export let BLOCKS = {
    name: i18n.t("ultrasonic.module_name"),
    id_prefix: "pnc_ultrasonic",
    blocks: {}
};

BLOCKS.blocks['measure'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("ultrasonic.measure"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port");
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_port = block.getFieldValue('port');

        var code = `(pnc.Ultrasonic("${dropdown_port}").measure())`;

        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};
