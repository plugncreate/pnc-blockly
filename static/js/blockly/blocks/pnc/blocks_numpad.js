import i18n from "../../../i18n";

export var BLOCKS = {
    name: i18n.t("numpad.name"),
    id_prefix: "pnc_numpad",
    blocks: {}
};

BLOCKS.blocks['get_button'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("numpad.get_button")) // "get pressed button on numpad on port"
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setOutput(true, "String");
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        var code = `pnc.Numpad_4x4("${dropdown_port}").get_button()`;
        return [code, Blockly.Python.ORDER_NONE];
    }
};

BLOCKS.blocks['wait_for_any'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("numpad.wait_for_any.part1"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port")
            .appendField(i18n.t("numpad.wait_for_any.part2"));
        this.setOutput(true, "String");
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        var code = `pnc.Numpad_4x4("${dropdown_port}").wait_for_any()`;
        return [code, Blockly.Python.ORDER_NONE];
    }
}
